plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("android.extensions")
}

version = "1.0.0"

android {
    compileSdkVersion(28)

    defaultConfig {
        minSdkVersion(22)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Config.Libs.supportSupportV4)
    implementation(Config.Libs.supportAppCompatV7)
    implementation(Config.Libs.supportConstraintLayout)
    implementation(Config.Libs.kotlin)
    implementation(Config.Libs.anko)
    implementation(Config.Libs.timber)
}
repositories {
    mavenCentral()
}
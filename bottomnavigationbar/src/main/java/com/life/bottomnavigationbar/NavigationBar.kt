package com.life.bottomnavigationbar

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import timber.log.Timber

class NavigationBar(ctx: Context, attrs : AttributeSet): LinearLayout(ctx, attrs) {

    private var listener : NavigationBarListener? = null
    private val buttonList = arrayListOf<NavigationButton>()

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams) {
        super.addView(child, index, params)
        registerItem(child as NavigationButton)
    }

    fun setNavigationBarListener(listener: NavigationBarListener?) {
        this.listener = listener
    }

    fun findButtonById(id : Int) = buttonList.find { it.id == id }

    @Deprecated(message = "Do not simulate a press-button....")
    fun simulatePressButton(buttonId : Int) {
        val button = buttonList.find { it.id == buttonId }
        button?.let { clickFunction(button) }
    }

    private fun clickFunction(view: View) {
        buttonList.forEach {
            it.isSelected = (it == view)
        }
        listener?.onItemClicked( view.id ) ?: run {
            Timber.w("No listener on the BottomNavigationBar")
        }
    }

    private fun registerItem(button : NavigationButton) {
        buttonList.add(button)
        button.setOnClickListener {
            clickFunction(it)
        }
    }
}

interface NavigationBarListener {
    fun onItemClicked(id : Int)
}

package com.life.bottomnavigationbar

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

class NavigationButton(ctx: Context, attrs : AttributeSet?): RelativeLayout(ctx, attrs) {

    private var clickListener : OnClickListener? = null
    private val buttonContainer : ViewGroup

    init {
        inflate(ctx, R.layout.navigation_button, this)

        buttonContainer = findViewById(R.id.button_container)

        val buttonImage = findViewById<ImageView>(R.id.button_image)
        val buttonLabel = findViewById<TextView>(R.id.button_label)
        val buttonSelector = findViewById<View>(R.id.button_selector)

        if (attrs != null) {
            val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.NavigationButton)

            styledAttributes.getDrawable(R.styleable.NavigationButton_image)?.let {
                buttonImage.setImageDrawable(it)
            }

            styledAttributes.getString(R.styleable.NavigationButton_text)?.let {
                buttonLabel.text = it
            }

            styledAttributes.getColorStateList(R.styleable.NavigationButton_text_color)?.let {
                buttonLabel.setTextColor(it)
            }

            styledAttributes.getDrawable(R.styleable.NavigationButton_selector_background)?.let {
               buttonSelector.background = it
            }

            styledAttributes.getColorStateList(R.styleable.NavigationButton_image_tint)?.let {
                buttonImage.imageTintList = it
            }

            styledAttributes.recycle()
         }

        buttonContainer.isClickable = true
        buttonContainer.isFocusable = true
    }

    constructor(ctx: Context) : this(ctx, null)

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) return true

        if (event.action == MotionEvent.ACTION_UP) {
            clickListener?.onClick(this)
        }
        return super.dispatchTouchEvent(event)
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        (0 until buttonContainer.childCount).forEach {
            buttonContainer.getChildAt(it).isSelected = selected
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        (0 until buttonContainer.childCount).forEach {
            buttonContainer.getChildAt(it).isEnabled = enabled
        }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        clickListener = l
    }
}